Public files for OPC UA.

The 'DEXPI OPC-UA Companion Specification' was officially released on 2021-09-10. These are the links to the official OPC UA documentation on opcfoundation.org:

Spec:                        https://opcfoundation.org/developer-tools/specifications-opc-ua-information-models/opc-ua-for-dexpi/

Schemas:                     https://files.opcfoundation.org/schemas/DEXPI/1.0/

Online Reference:            https://reference.opcfoundation.org/DEXPI/docs/

